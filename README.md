# Arduino Tetris #

Projekt wykonany na przedmiot "Systemy Wbudowane", prowadzący: mgr inż. Bartosz Zgrzeba
Autor projektu: Marcin Borzymowski, nr indeksu: 113994
Termin oddania: 23.01.2016

## Opis ##

Przeniesienie kultowej gry Tetris na Arduino

## Wykorzystane biblioteki ##

* NewliquidCrystal 1.3.4 --> LiquidCrystal_I2C.h (https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads)
* LedControl.h (http://playground.arduino.cc/Main/LedControl)

## Wykorzystane elementy ##

* Arduino UNO R3 chiński klon
* płytka stykowa
* LCD HD44780 niebieski negatyw
* konwerter do I2C dla HD44780
* 2x matryca LED czerwona
* 2x sterownik led MAX7219
* Buzzer pasywny
* 5x pushbutton
* 5x opornik
* zworki i kable
* włącznik ze wzmacniacza Logitech
* bateria 9V
* kartony po piwie Desperados i koszulkach na dokumenty Bantex

## Filmy przedstawiające działanie ##

* Efekt końcowy: https://www.youtube.com/watch?v=PImkTPu7KZU

* Pierwsza, niedziałająca wersja: https://www.youtube.com/watch?v=L5HUwz9lob4
* Kasowanie linii: https://www.youtube.com/watch?v=a4gtOuiTFyA

## Notatki z prac ##

* Tam gdzie się da używam operacji bitowych, żeby było szybciej
* ledcontrole można podłączyć pod ISCP. Dla Arduino Uno takie piny są tak zamapowane (pin_ISCP=pin_normalny):   1=12,    2=Vcc,    3=13,    4=11,    5=Reset,    6=GND
* Lepiej nie ustawiać jasności na więcej niż 5, żeby się diody nie zjarały przy ciągłym świeceniu
* Nad AREF są SDA i SCL wyprowadzone, ale jak się ich używa, to nie można ruszać analogowych 4 i 5