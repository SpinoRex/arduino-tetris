#include "LiquidCrystal_I2C.h"
#include "LedControl.h"

//==============================================================================
//Tetris na Arduino UNO v1.0.9
//Projekt wykonany na przedmiot "Systemy Wbudowane", prowadzący: mgr inż. Bartosz Zgrzeba
//
//Autor projektu: Marcin Borzymowski, nr indeksu: 113994
//
//Termin oddania: 23.01.2016
//
//
//Moje notatki:
//    - Tam gdzie się da używam operacji bitowych, żeby było szybciej
//    - ledcontrole można podłączyć pod ISCP. Dla Arduino Uno takie piny są tak zamapowane (pin_ISCP=pin_normalny):   1=12,    2=Vcc,    3=13,    4=11,    5=Reset,    6=GND
//    - Lepiej nie ustawiać jasności na więcej niź 5, żeby się diody nie zjarały przy ciągłym świeceniu
//    - Nad AREF są SDA i SCL wyprowadzone, ale jak się ich używa, to nie można ruszać analogowych 4 i 5
//
//==============================================================================


LedControl lc=LedControl(13,11,12,2);                                    //Piny: Data in, clk ,chip select, ilość ledcontroli
LiquidCrystal_I2C lcd(0x27, 2,  1,  0,  4,  5,  6,  7,  3, POSITIVE);    //LCD przez I2C na układzie PCF8574T. 0x27 - adres tego konkretnego urządzenia, dalej układ pinów

//Ekrany wirtualne
int lc0[] = {0, 0, 0, 0, 0, 0, 0, 0};                              //Górny ledcontrol
int lc1[] = {0, 0, 0, 0, 0, 0, 0, 0};                              //Dolny ledcontrol
long active[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};  //Aktywny obraz
long screen[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};  //Nieaktywny obraz (pod spodem)
int tmpCol = 0;                                                    //Tymczasowa kolumna, potrzebna przy kasowaniu linii

//Zwykłe zmienne
int figura = 0;                                                      //Numer wylosowanej figury
int figuraNext = 0;                                                  //Numer wylosowanej figury na następny przebieg
int fromLeft = 0;                                                    //Odległość figury od lewej krawędzi
int fromRight = 0;                                                   //Odległość figury od prawej krawędzi
int angle = 0;                                                       //Kąt obrotu figury
int colCheck = 0;                                                    //Licznik do weryfikacji przesunięcia kolumn w dół
int moveCheck = 0;                                                   //Licznik do weryfikacji przesunięcia figury w bok
int score = 0;                                                       //Wynik
int started = 0;                                                     //Zmienna do poruszania się w loop()
int lcRows = 16;                                                     //Ile wierszy - mamy 2 ledcontrole w pionie, po 8 wierszy, więc 16
int lcCols = 8;                                                      //Ile kolumn - mamy 1 ledcontrol w poziomie, po 8 kolumn, więc 8
int allLines = 0;                                                    //Licznik wszystkich skasowanych linii
int currLines = 0;                                                   //Licznik aktualnie usuniętych linii - do punktacji:  1 linia = 1pkt,  2 linie = 4pkt,  3 linie = 16pkt, 4 linie = 64pkt
int brickDelay = 0;                                                  //Opóźnienie spadania klockow, zmienne w trakcie programu
int defDelay = 500;                                                  //Domyślne opóźnienie
int level = 0;                                                       //Poziom 0 - 5. Zwiększa się po skasowaniu 100 linii

//Piny
int speaker_pin = 7;         //Wyjście na głośnik
int rotate_button = 2;       //Wejście z guzika obracania
int down_button = 4;         //Wejście z guzika "W dół"
int right_button = 3;        //Wejście z guzika "W prawo"
int left_button = 5;         //Wejście z guzika "W lewo"
int start_button = 6;        //Wejście z guzika "Start/restart"

//Własne znaki na LCD
byte X[8] = 
{
  0b00000,
  0b10001,
  0b01010,
  0b00100,
  0b01010,
  0b10001,
  0b00000,
  0b00000
};

byte O[8] = 
{
  0b00000,
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b00000,
  0b00000
};

byte L[8] = 
{
  0b11000,
  0b11000,
  0b11000,
  0b11000,
  0b11000,
  0b11111,
  0b11111,
  0b00000
};

byte J[8] = 
{
  0b00011,
  0b00011,
  0b00011,
  0b00011,
  0b00011,
  0b11111,
  0b11111,
  0b00000
};

byte T[8] = 
{
  0b00000,
  0b00000,
  0b11111,
  0b11111,
  0b01110,
  0b01110,
  0b00000,
  0b00000
};

byte I[8] = 
{
  0b01100,
  0b01100,
  0b01100,
  0b01100,
  0b01100,
  0b01100,
  0b01100,
  0b00000
};

byte Z[8] = 
{
  0b00000,
  0b00000,
  0b11110,
  0b11110,
  0b01111,
  0b01111,
  0b00000,
  0b00000
};

byte S[8] = 
{
  0b00000,
  0b00000,
  0b01111,
  0b01111,
  0b11110,
  0b11110,
  0b00000,
  0b00000
};

//Nuty
int length = 99;   //Ilość nut
char notes[] = "EbCDCbaaCEDCbbCDECaa DFAGFEECEDCbbCDECaa EbCDCbaaCEDCbbCDECaa DFAGFEECEDCbbCDECaa ECDbCab ECDbCEAJ ";   //Tetrisowa muza --> "Korobeiniki";  Spacja to pauza
int beats[] =      //Czasy
{ 
  2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 2, 2, 2, 4, 2, 
  2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 2, 2, 2, 4, 1, 
  2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 2, 2, 2, 4, 2, 
  2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 2, 2, 2, 4, 1, 
  5, 5, 5, 5, 5, 5, 7, 2, 5, 5, 5, 5, 2, 2, 5, 5, 3
};  
int tempo = 128;        //Tempo



//Granie dźwięku - to wziąłem z exampli Arduino
void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(speaker_pin, HIGH);
    delayMicroseconds(tone);
    digitalWrite(speaker_pin, LOW);
    delayMicroseconds(tone);
  }
}


//Granie nuty - to wziąłem z exampli Arduino
void playNote(char note, int duration) {
  char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' , 'D', 'E', 'F', 'G', 'J', 'A', 'B'};
  //int tones[] = { 3830, 3400, 3038, 2864, 2550, 2272, 2028, 1915, 1700, 1519, 1432, 1275, 1205, 1136, 1014 };
  int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956, 850, 760, 716, 637, 603, 568, 507 };
  //int tones[] = { 956, 850, 760, 716, 637, 568, 507, 478, 425, 380, 358, 318, 302, 284, 253 };
 
  //Odtwarzanie dźwięku odpowiadającego nucie
  for (int i = 0; i < 14; i++) {
    if (names[i] == note) {
      playTone(tones[i], duration);
    }
  }
}



//Wiodące zera na LCD
void lcdPrintWithLeadingZeros(LiquidCrystal_I2C lcd, int value, int startCol, int row)
{
     lcd.setCursor(startCol, row);
     if(value < 10)
     {
         lcd.print("000");
         lcd.setCursor(startCol+3, row);
     }
     else if(value < 100)
     {
         lcd.print("00");
         lcd.setCursor(startCol+2,row);
     }
     else if(value < 1000)
     {
         lcd.print("0");
         lcd.setCursor(startCol+1,row);
     }
     lcd.print(value);
}



//Aktualizacja kolumny na ledcontrolach
void updateColumn(int colnum)
{ 
    lc0[colnum] = active[colnum] >> (lcRows / 2);                            //Wyswietlimy na lc0 aktywny obraz przesunięty o ilość wierszy w pojedynczym ledcontrolu
    lc1[colnum] = active[colnum];                                            //Wyświetlimy na lc1 aktywny obraz (tyle ile się zmieści dolnych bitów)
    lc.setColumn(0,colnum,(screen[colnum] >> (lcRows / 2)) | lc0[colnum]);   //Wyświetlamy na lc0 aktywny obraz nałożony na tło przesunięte o ilość wierszy w pojedynczym ledcontrolu
    lc.setColumn(1,colnum,screen[colnum] | lc1[colnum]);                     //Wyświetlamy na lc1 aktywny obraz nałożony na tło
}



//Opóźnienie guzika
void buttonDelay(int bdelay)
{
    if(brickDelay > bdelay)          //Zmniejszenie opóźnienia, żeby zbytnio nie wydłużać kroku
    {
        brickDelay -= bdelay;
    }
    delay(bdelay);
}

                         
//Ekran powitalny
void splashScreen()
{
     int up[] =    //Górny ledcontrol
     {
         B11101110,   //   o o o   o o o  
         B01001000,   //     o     o   
         B01001100,   //     o     o o  
         B01001000,   //     o     o   
         B01001110,   //     o     o o o 
         B00000000,   //           
         B11101110,   //   o o o   o o o  
         B01001010    //     o     o   o 
     };
 
     int down[] =    //Dolny ledcontrol
     {
         B01001100,   //     o     o o   
         B01001010,   //     o     o   o 
         B01001001,   //     o     o     o
         B00000000,   //           
         B01000111,   //     o       o o o 
         B01000100,   //     o       o     
         B01000010,   //     o         o   
         B01001110    //     o     o o o   
     };

     for(int rownum = 0; rownum < 8; rownum++)
     {
         lc.setRow(0,rownum,up[rownum]);     //Wgranie treści z tablicy wierszami
         lc.setRow(1,rownum,down[rownum]);   //Wgranie treści z tablicy wierszami
     }
}





//SETUP
void setup() {
  pinMode(speaker_pin, OUTPUT);  
  pinMode(rotate_button, INPUT);
  pinMode(down_button, INPUT);
  pinMode(right_button, INPUT);
  pinMode(left_button, INPUT);
  pinMode(start_button, INPUT);

  //Włączenie ledcontroli
  lc.shutdown(0,false);
  lc.shutdown(1,false);

  //Ustawienie jasności
  lc.setIntensity(0,5);
  lc.setIntensity(1,5);

  //Wyczyszczenie ledcontroli
  lc.clearDisplay(0);
  lc.clearDisplay(1);

  //seed z szumu na nieużywanym analogowym wejściu
  randomSeed(analogRead(0));   

  //Inicjalizacja elcedeka
  lcd.begin(16,2);          //16 znaków, 2 wiersze

  //Tworzenie znaczków figur dla LCD
  lcd.createChar(0, X);
  lcd.createChar(1, O);
  lcd.createChar(2, L);
  lcd.createChar(3, J);
  lcd.createChar(4, T);
  lcd.createChar(5, I);
  lcd.createChar(6, Z);
  lcd.createChar(7, S);
  
  //Powitanie na LCD
  lcd.setCursor(1,0);
  lcd.print("ARDUINO TETRIS");
  lcd.setCursor(1,1);
  lcd.print("by SRX, v1.0.9");
}


//LOOP
void loop() 
{  

//==============================================================================
//==POWITANIE===================================================================
//==============================================================================

 if(started == 0)   //Jeśli gra nie jest wystartowana
 { 

     //Ekran powitalny*/
     splashScreen();

     //Melodyjka
     //Stan guzika trzeba sprawdzać w każdym obrocie, inaczej trzeba byłoby czekać, aż się cała melodia odegra
     for (int i = 0; i < length; i++) 
     {
         if(digitalRead(start_button) == HIGH)   //Jeśli jest wciśnięty "Start/restart"
         {
             started = 1;                        //Zmiana flagi - Będziemy startować grę
             break;                              //Wychodzimy z odgrywania melodyjki
         }
         if (notes[i] == ' ') 
         {
             delay(beats[i] * tempo);   //Pauza
         } 
         else 
         {
             playNote(notes[i], beats[i] * tempo);   //Graj nutę
         }

         //Pauza pomiędzy kolejnymi nutami
        delay(tempo / 2); 
     }
 }



//==============================================================================
//==GRA=========================================================================
//==============================================================================

 else   //Jeśli gra wystartowana
 {

     //Wyczyszczenie ekranów fizycznych i wirtualnych
     lc.clearDisplay(0);
     lc.clearDisplay(1); 
     memset(lc0, 0, sizeof(lc0));
     memset(lc1, 0, sizeof(lc1));
     memset(active, 0, sizeof(active));
     memset(screen, 0, sizeof(screen));
     tmpCol = 0;
     lcd.clear();


     while(started == 1)   //Dopóki gra wystartowana
     {
         //Wypisanie skasowanych linii na LCD
         lcd.setCursor(0,0);
         lcd.print("LINES:");
         lcdPrintWithLeadingZeros(lcd, allLines, 6, 0);


         //Wypisanie punktów na LCD
         lcd.setCursor(0,1);
         lcd.print("SCORE:");
         lcdPrintWithLeadingZeros(lcd, score, 6, 1);


         //Poziom
         if(allLines < 100)         
         {
             level = 0;              //Level 0 dla 0 - 99 linii
         }
         else if(allLines < 200)
         {
             level = 1;              //Level 1 dla 100 - 199 linii
         }
         else if(allLines < 300)    
         {   
             level = 2;              //Level 2 dla 200 - 299 linii
         }
         else if(allLines < 400)
         {
             level = 3;              //Level 3 dla 300 - 399 linii
         }
         else if(allLines < 500)
         {
             level = 4;              //Level 4 dla 400 - 499 linii
         }
         else
         {
             level = 5;              //Level 5 powyżej 500 linii
         }

         //Wyświetlenie levelu na LCD
         lcd.setCursor(11,0);
         lcd.print("LVL:");
         lcd.setCursor(15,0);
         lcd.print(level);


         
         //Reset opóźnień
         defDelay = (5 - level) * 100;
         brickDelay = defDelay;


      
         //Losowanie figury --> od 1 do 8, czyli może dać 1, 2, 3, 4, 5, 6, 7
         if(figura == 0)                 //Jeśli figura == 0, to znaczy, że to pierwszy bieg i nie było jeszcze losowań, więc trzeba jakąś wylosować
         {
              figura = random(1,8);    //Losowanie bieżącej, bo też jeszcze nie ma
         }
         else
         {
             figura = figuraNext;        //Bieżącą figurą ma być ta, która była wylosowana w poprzednim przebiegu
         }
         figuraNext = random(1,8);       //Losowanie następnej figury
         angle = 0;                      //Kąt obrotu. Na początku nowej figury zawsze jest to 0

         //Następna figura na LCD
         lcd.setCursor(11,1);
         lcd.print("NXT:");
         lcd.setCursor(15,1);
         lcd.write(byte(figuraNext));
         


         //Tworzenie bieżącej figury
         switch(figura)
         {
             case 1:         
             //"O"
                 active[3] = 131072 + 65536;
                 active[4] = 131072 + 65536;
                 fromLeft = 3;
                 fromRight = 3;
                 break;
                 
             case 2:         
             //"L"
                 active[3] = 262144 + 131072 + 65536;
                 active[4] = 65536;
                 fromLeft = 3;
                 fromRight = 3;
                 break;
                 
             case 3:
             //"J"
                 active[3] = 65536;
                 active[4] = 262144 + 131072 + 65536;
                 fromLeft = 3;
                 fromRight = 3;
                 break;
                 
             case 4:         
             //"T"
                 active[2] = 131072;
                 active[3] = 131072 + 65536;
                 active[4] = 131072;
                 fromLeft = 2;
                 fromRight = 3;
                 break;
                 
             case 5:         
             //"I"
                 active[3] = 524288 + 262144 + 131072 + 65536;
                 fromLeft = 3;
                 fromRight = 4;
                 break;
                 
             case 6:         
             //"Z"
                 active[2] = 131072;
                 active[3] = 131072 + 65536;
                 active[4] = 65536;
                 fromLeft = 2;
                 fromRight = 3;
                 break;
                 
             case 7:
             //"S"
                 active[2] = 65536;
                 active[3] = 131072 + 65536;
                 active[4] = 131072;
                 fromLeft = 2;
                 fromRight = 3;
                 break;
         }



//==============================================================================
//==Cała akcja na wyświetlaczu==================================================
//==============================================================================

         //Spadanie figury
         for(int krok = 0; krok < lcRows + 1; krok++)   //Max 16 kroków może być, bo są 2 ledcontrole po 8 ledów,  + 1 na "cwaniackie przesuwanie"
         {
             colCheck = 0;  //Zerujemy sprawdzacza kolumn na początku spadania nowej figury



//==Przesunięcie w lewo=========================================================

             //Przesuń w lewo
             for(int i = 0; i < (lcCols / 2); i++)                                                            //Możliwe max 4 przesunięcia na krok
             {
                 if((digitalRead(left_button) == HIGH) && (fromLeft > 0))                                     //Jeśli guzik "W lewo" jest wciśnięty i figura nie jest przy lewej krawędzi
                 {
                     moveCheck = 0;                                                                           //Zerowanie sprawdzacza przesunięcia w bok
                     for(int colnum = fromLeft; colnum < (lcCols - fromRight); colnum++)                      //Pętla po kolumnach, które potencjalnie mogą wziąć udział w przesuwaniu
                     {
                         if((active[colnum] & screen[colnum - 1]) == 0)                                       //Maska bitowa --> Jeśli kolumna figury i tła (o jeden w lewo) po iloczynie dają 0, to ok (nie nakładają się)
                         {
                             moveCheck++;                                                                     //Zwiększamy sprawdzacza
                         }
                     }

                     if(moveCheck == (lcCols - fromLeft - fromRight))                                         //Jeśli zweryfikowano tyle kolumn, ile wynosi szerokość figury
                     {
                         for(int colnum = (fromLeft - 1); colnum < (lcCols - fromRight); colnum++)            //Pętla po wszystkich kolumnach do przesunięcia
                         {
                             if(colnum < (lcCols - 1))                                                        //Jeśli to nie jest ostatnia kolumna (indeks 7)
                             {
                                 active[colnum] = active[colnum+1];                                           //Przesuwanie kolumn
                             }
                             else
                             {
                                 active[colnum] = 0;                                                          //Jeśli jest to ostatnia kolumna, to zerujemy, bo nie ma skąd przesunąć
                             }
                             updateColumn(colnum);                                                            //Aktualizacja ekranu
                         }
                         fromLeft--;                                                                          //Zmniejsza się odległość od lewej krawędzi
                         fromRight++;                                                                         //Zwiększa się odległość od prawej krawędzi
                         playNote('E', 10);
                         buttonDelay(200);                                                                    //Opóźnienie, żeby nie przesuwać za wiele za jednym naciśnięciem buttona
                     }
                 }
             }



//==Przesunięcie w prawo========================================================

             //Przesuń w prawo
             for(int i = 0; i < (lcCols / 2); i++)                                                            //Możliwe max 4 przesunięcia na krok
             {
                 if((digitalRead(right_button) == HIGH) && (fromRight > 0))                                   //Jeśli guzik "W prawo" jest wciśnięty i figura nie jest przy prawej krawędzi
                 {
                     moveCheck = 0;                                                                           //Zerowanie sprawdzacza przesunięcia w bok
                     for(int colnum = fromLeft; colnum < (lcCols - fromRight); colnum++)                      //Pętla po kolumnach, które potencjalnie mogą wziąć udział w przesuwaniu
                     {
                         if((active[colnum] & screen[colnum + 1]) == 0)                                       //Maska bitowa --> Jeśli kolumna figury i tła (o jeden w prawo) po iloczynie dają 0, to ok (nie nakładają się)
                         {
                             moveCheck++;                                                                     //Zwiększamy sprawdzacza
                         }
                     }
 
                     if(moveCheck == (lcCols - fromLeft - fromRight))                                         //Jeśli zweryfikowano tyle kolumn, ile wynosi szerokość figury    
                     {
                         for(int colnum = (lcCols - fromRight); colnum > (fromLeft - 1); colnum--)            //Pętla po wszystkich kolumnach do przesunięcia (ale od tyłu, bo przesuwamy w prawo)
                         {
                             if(colnum > 0)                                                                   //Jeśli to nie jest pierwsza kolumna (indeks 0)
                             {
                                 active[colnum] = active[colnum-1];                                           //Przesuwanie kolumn
                             }
                             else      
                             {
                                 active[colnum] = 0;                                                          //Jeśli jest to pierwsza kolumna, to zerujemy, bo nie ma skąd przesunąć
                             }
                             updateColumn(colnum);                                                            //Aktualizacja ekranu
                         }
                         fromLeft++;
                         fromRight--;
                         playNote('E', 10);
                         buttonDelay(200);                                                                    //Opóźnienie, żeby nie przesuwać za wiele za jednym naciśnięciem buttona
                     }
                 }
             }



//==Przyspieszenie spadania=====================================================

             //Guzik w dól
             if(digitalRead(down_button) == HIGH)                       //Jeśli guzik "W dół" jest wciśnięty
             {
                 brickDelay = 0;                                        //Wyłączamy opóźnienie spadania
                 playNote('b', 10);
             }
             else                                                       //W przeciwnym wypadku
             {
                 brickDelay = defDelay;                                      //Ustawiamy opóźnienie spadania
             }



//==Obrót klocka================================================================

             //Obrót
             for(int i = 0; i < (lcCols / 2); i++)
             {
                 if(digitalRead(rotate_button) == HIGH)                //Jeśli guzik "Obrót" jest wciśnięty
                 {                     
                     //Obrót jest inny w zależności od figury
                     //Wszystkie figury z wyjątkiem "O" i "I" są wpisane w kwadrat 3x3 i obracają się wg środkowego pola - tak było w oryginale z 1984r.
                     //Ze względu na to, że dużo przy obracaniu manipuluję bitami, to żeby łatwiej było zrozumieć, będę w komentarzach odnosił się do pół figury w taki sposób:
                     //
                     // 1 2 3
                     // 4 5 6
                     // 7 8 9
                     //
                     //Dla "I" będzie inaczej:
                     //
                     //  1  2  3  4
                     //  5  6  7  8
                     //  9 10 11 12
                     // 13 14 15 16
                     //
                     //Jak będę graficznie przedstawiał figurę, "o" będzie oznaczać zaświeconą diodę, a "." nie
                     //
                     switch(figura)
                     {
                         case 1:
                         //"O"
                             break;                                                                                                                    //Nic nie robimy, kostki nie ma po co obracać, bo po obróceniu wyglądałaby tak samo

                         case 2:
                         //"L" 
                             switch(angle)                                                                                                             //Sprawdzamy aktualny kąt
                             {
                                 case 0:
                                 // . o .        . . .
                                 // . o .  --->  o o o
                                 // . o o        o . .
                                     if((fromLeft > 0)                                                                                                 //Będziemy dodawać kolumnę z lewej, więc sprawdzamy, czy nie jesteśmy przy krawędzi
                                     && (((active[fromLeft + 1] | (active[fromLeft + 1] << 1)) & screen[fromLeft - 1]) == 0))                          //Czy pola 4 i 7 są puste
                                     {
                                         active[fromLeft - 1] = (active[fromLeft + 1] | (active[fromLeft + 1] << 1));                                  //Włączamy pola 4 i 7
                                         updateColumn(fromLeft - 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft] = (active[fromLeft + 1] << 1);                                                               //Wyłączamy pola 2 i 8, 5 nadal się świeci
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = (active[fromLeft + 1] << 1);                                                           //Włączamy 6, wyłączamy 9
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         fromLeft--;                                                                                                   //Zmniejsza się odległość od lewej (doszła nowa kolumna)
                                         angle = 1;                                                                                                    //Bieżący kąt to 1
                                     }
                                     break;

                                 case 1:
                                 // . . .        o o .
                                 // o o o  --->  . o .
                                 // o . .        . o .
                                     if((((active[fromLeft + 2] << 1) & screen[fromLeft]) == 0)                                                        //Czy pole 1 jest puste
                                     && ((((active[fromLeft + 1] << 1) | (active[fromLeft + 1] >> 1)) & screen[fromLeft + 1]) == 0))                   //Czy pola 2 i 8 są puste
                                     {
                                         active[fromLeft] = (active[fromLeft + 2] << 1);                                                               //Włączamy pole 1, wyłączamy 4 i 7
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = active[fromLeft + 1] | (active[fromLeft + 1] << 1) | (active[fromLeft + 1] >> 1);      //Włączamy pola 2 i 8, pole 5 nadal świeci
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumy
                                         active[fromLeft + 2] = 0;                                                                                     //Wyłączamy całą ostatnią kolumnę
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromRight++;                                                                                                  //Zwiększa się odległość od prawej krawędzi
                                         angle = 2;                                                                                                    //Bieżący kąt to 2
                                     }
                                     break;

                                 case 2:
                                 // o o .        . . o
                                 // . o .  --->  o o o
                                 // . o .        . . .
                                     if((fromRight > 0)                                                                                                //Będziemy dodawać kolumnę z prawej, więc sprawdzamy, czy nie jesteśmy przy krawędzi
                                     && (((active[fromLeft] >> 1) & screen[fromLeft]) == 0)                                                            //Czy pole 4 jest puste
                                     && ((((active[fromLeft + 1] << 1) & active[fromLeft + 1]) & screen[fromLeft + 1]) == 0))                          //Czy pole 5 jest puste
                                     {
                                         active[fromLeft] = (active[fromLeft] >> 1);                                                                   //Wyłączamy pole 1, włączamy 4
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = active[fromLeft];                                                                      //Wyłączamy pola 2 i 8, 5 zostaje
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = ((active[fromLeft + 1] << 1) | active[fromLeft + 1]);                                  //Włączamy pola 3 i 6
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromRight--;                                                                                                  //Zmniejsza się odległość od prawej krawędzi
                                         krok--;                                                                                                       //Nie ma już żadnych włączonych pól w dolnym wierszu, więc zmniejszamy krok
                                         angle = 3;                                                                                                    //Bieżący kąt to 3
                                     }
                                     break;

                                 case 3:
                                 // . . o        . o .
                                 // o o o  --->  . o .
                                 // . . .        . o o
                                     if(((((active[fromLeft] << 1) | (active[fromLeft] >> 1)) & screen[fromLeft + 1]) == 0)                            //Czy pola 2 i 8 są puste
                                     && (((active[fromLeft] >> 1) & screen[fromLeft + 2]) == 0)                                                        //Czy pole 9 jest włączone
                                     && (krok < lcRows))                                                                                               //Czy nie jest to ostatni krok - bo będziemy dodawać wiersz na dole
                                     {
                                         active[fromLeft] = 0;                                                                                         //Wyłączamy całą pierwszą kolumnę
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = (active[fromLeft + 2] | (active[fromLeft + 2] >> 1));                                  //Włączamy pola 2 i 8
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = ((active[fromLeft + 2] >> 1) & (active[fromLeft + 2] >> 2));                           //Włączamy pole 9, wyłączamy 3 i 6
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromLeft++;                                                                                                   //Zwiększa się odległość od lewej krawędzi
                                         krok++;                                                                                                       //Dołożyliśmy wiersz na dole, więc trzeba zwiększyć krok
                                         angle = 0;                                                                                                    //Bieżący kąt to 0
                                     }
                                     break;
                             }
                             break;

                         case 3:
                         //"J"
                             switch(angle)                                                                                                             //Sprawdzamy aktualny kąt
                             {
                                 case 0:
                                 // . o .        o . .
                                 // . o .  --->  o o o
                                 // o o .        . . .
                                     if((fromRight > 0)                                                                                                //Będziemy dodawać kolumnę z prawej, więc sprawdzamy, czy nie jesteśmy przy krawędzi
                                     && ((((active[fromLeft] << 2) | (active[fromLeft] << 1)) & screen[fromLeft]) == 0)                                //Czy pola 1 i 4 są puste
                                     && (((active[fromLeft] << 1) & screen[fromLeft + 2]) == 0))                                                       //Czy pole 6 jest puste
                                     {
                                         active[fromLeft] = ((active[fromLeft] << 2) | (active[fromLeft] << 1));                                       //Włączamy pola 1 i 4, wyłączamy 7
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = ((active[fromLeft + 1] << 1) & (active[fromLeft + 1] >> 1));                           //Wyłączamy pola 2 i 8
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = active[fromLeft + 1];                                                                  //Włączamy pole 6
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromRight--;                                                                                                  //Zmniejsza się odległość od prawej krawędzi
                                         krok--;                                                                                                       //Nie ma już żadnych włączonych pól w dolnym wierszu, więc zmniejszamy krok
                                         angle = 1;                                                                                                    //Bieżący kąt to 1
                                     }
                                     break;

                                 case 1:
                                 // o . .        . o o
                                 // o o o  --->  . o .
                                 // . . .        . o .
                                     if((krok < lcRows)                                                                                                //Czy nie jest to ostatni krok - bo będziemy dodawać wiersz na dole
                                     && ((((active[fromLeft + 1] << 1) | (active[fromLeft + 1] >> 1)) & screen[fromLeft + 1]) == 0)                    //Czy pola 2 i 8 są puste
                                     && (((active[fromLeft + 2] << 1) & screen[fromLeft + 2]) == 0))                                                   //Czy pole 3 jest puste
                                     {
                                         active[fromLeft] = 0;                                                                                         //Wyłączamy całą pierwszą kolumnę
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = (active[fromLeft + 1] | (active[fromLeft + 1] << 1) | (active[fromLeft + 1] >> 1));    //Włączamy pola 2 i 8
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = (active[fromLeft + 2] << 1);                                                           //Włączamy pole 3, wyłączamy 6
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromLeft++;                                                                                                   //Zwiększa się odległość od lewej krawędzi
                                         krok++;                                                                                                       //Dołożyliśmy wiersz na dole, więc trzeba zwiększyć krok
                                         angle = 2;                                                                                                    //Bieżący kąt to 2
                                     }
                                     break;

                                 case 2:
                                 // . o o        . . .
                                 // . o .  --->  o o o
                                 // . o .        . . o
                                     if((fromLeft > 0)                                                                                                 //Będziemy dodawać kolumnę z lewej, więc sprawdzamy, czy nie jesteśmy przy krawędzi
                                     && (((active[fromLeft + 1] >> 1) & screen[fromLeft - 1]) == 0)                                                    //Czy pole 4 jest puste
                                     && ((((active[fromLeft + 1] >> 1) | (active[fromLeft + 1] >> 2)) & screen[fromLeft + 1]) == 0))                   //Czy pola 6 i 9 są puste
                                     {
                                         active[fromLeft - 1] = (active[fromLeft + 1] >> 1);                                                           //Włączamy pole 4
                                         updateColumn(fromLeft - 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft] = active[fromLeft - 1];                                                                      //Wyłączamy pola 2 i 8
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = (active[fromLeft] | (active[fromLeft + 1] >> 2));                                      //Wyłączamy pole 3, włączamy 6 i 9
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         fromLeft--;                                                                                                   //Zmniejsza się odległość od lewej (doszła nowa kolumna)
                                         angle = 3;                                                                                                    //Bieżący kąt to 3
                                     }
                                     break;

                                 case 3:
                                 // . . .        . o .
                                 // o o o  --->  . o .
                                 // . . o        o o .
                                     if((((active[fromLeft] >> 1) & screen[fromLeft]) == 0)                                                            //Czy pole 7 jest puste
                                     && ((((active[fromLeft] << 1) | (active[fromLeft >> 1])) & screen[fromLeft + 1]) == 0))                           //Czy pola 2 i 8 są puste
                                     {
                                         active[fromLeft] = (active[fromLeft] >> 1);                                                                   //Włączamy pole 7, wyłączamy 4
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = ((active[fromLeft + 1] << 1) | active[fromLeft + 2]);                                  //Włączamy pola 2 i 8
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = 0;                                                                                     //Wyłączamy całą ostatnią kolumnę
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromRight++;                                                                                                  //Zwiększa się odległość od prawej krawędzi
                                         angle = 0;                                                                                                    //Bieżący kąt to 0
                                     }
                                     break;
                             }
                             break;

                         case 4:
                         //"T"
                             switch(angle)                                                                                                             //Sprawdzamy aktualny kąt
                             {
                                 case 0:
                                 // . . .        . o .
                                 // o o o  --->  o o .
                                 // . o .        . o .
                                     if(((active[fromLeft + 1] << 1) & screen[fromLeft + 1]) == 0)                                                     //Czy pole 2 jest puste
                                     {
                                         //active[fromLeft]                                                                                            //Tu się nic nie zmienia
                                         active[fromLeft + 1] = active[fromLeft + 1] | (active[fromLeft + 1] << 1);                                    //Włączamy pole 2
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = 0;                                                                                     //Wyłączamy całą ostatnią kolumnę
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromRight++;                                                                                                  //Zwiększa się odległość od prawej krawędzi
                                         angle = 1;                                                                                                    //Bieżący kąt to 1
                                     }
                                     break;
                                 
                                 case 1:
                                 // . o .        . o .
                                 // o o .  --->  o o o
                                 // . o .        . . .
                                     if((fromRight > 0)                                                                                                //Będziemy dodawać kolumnę z prawej, więc sprawdzamy, czy nie jesteśmy przy krawędzi
                                     && ((active[fromLeft] & screen[fromLeft + 2])== 0))                                                               //Czy pole 6 jest puste
                                     {
                                         //active[fromLeft]                                                                                            //Tu się nic nie zmienia
                                         active[fromLeft + 1] = active[fromLeft + 1] & (active[fromLeft + 1] << 1);                                    //Wyłączamy pole 8
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = active[fromLeft];                                                                      //Włączamy pole 6
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromRight--;                                                                                                  //Zmniejsza się odległość od prawej krawędzi
                                         krok--;                                                                                                       //Nie ma już żadnych włączonych pól w dolnym wierszu, więc zmniejszamy krok
                                         angle = 2;                                                                                                    //Bieżący kąt to 2
                                     }
                                     break;
                                 
                                 case 2:
                                 // . o .        . o .
                                 // o o o  --->  . o o
                                 // . . .        . o . 
                                     if((((active[fromLeft + 1] >> 1) & screen[fromLeft + 1]) == 0)                                                    //Czy pole 8 jest puste
                                     && (krok < lcRows))                                                                                               //Czy nie jest to ostatni krok - bo będziemy dodawać wiersz na dole
                                     {
                                         active[fromLeft] = 0;                                                                                         //Wyłączamy całą pierwszą kolumnę
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = active[fromLeft + 1] | (active[fromLeft + 1] >> 1);                                    //Włączamy pole 8
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         //active[fromLeft + 2]                                                                                        //Tu się nic nie zmienia
                                         fromLeft++;                                                                                                   //Zwiększa się odległość od lewej krawędzi
                                         krok++;                                                                                                       //Dołożyliśmy wiersz na dole, więc trzeba zwiększyć krok
                                         angle = 3;                                                                                                    //Bieżący kąt to 3
                                     }
                                     break;
                                 
                                 case 3:
                                     if((fromLeft > 0)                                                                                                 //Będziemy dodawać kolumnę z lewej, więc sprawdzamy, czy nie jesteśmy przy krawędzi
                                     && ((active[fromLeft + 1] & screen[fromLeft - 1]) == 0))                                                          //Czy pole 4 jest puste
                                     {
                                         active[fromLeft - 1] = active[fromLeft + 1];                                                                  //Włączamy pole 4 
                                         updateColumn(fromLeft - 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft] = active[fromLeft] & (active[fromLeft] >> 1);                                                //Wyłączamy pole 2
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         //active[fromLeft + 1]                                                                                        //Tu się nic nie zmienia
                                         fromLeft--;                                                                                                   //Zmniejsza się odległość od lewej krawędzi
                                         angle = 0;                                                                                                    //Bieżący kąt to 0
                                     }
                                     break;
                             }
                             break;

                         case 5:
                         //"I"
                             switch(angle)                                                                                                             //Sprawdzamy aktualny kąt
                             {
                                 case 0:
                                 // . o . .        . . . .
                                 // . o . .  --->  o o o o
                                 // . o . .        . . . .
                                 // . o . .        . . . .
                                     if((fromLeft > 0)                                                                                                 //Będziemy dodawać kolumnę z lewej, więc sprawdzamy, czy nie jesteśmy przy krawędzi
                                     && (fromRight > 1)                                                                                                //Będziemy dodawać 2 kolumny z prawej, więc sprawdzamy, czy nie jesteśmy przy krawędzi lub obok
                                                                                                                                                       //Czy pola 5, 7 i 8 są puste
                                     && ((((((active[fromLeft] >> 1) & (active[fromLeft] << 2)) & screen[fromLeft - 1]) & screen[fromLeft + 1]) & screen[fromLeft + 2]) == 0))
                                     {
                                         active[fromLeft - 1] = ((active[fromLeft] >> 1) & (active[fromLeft] << 2));                                   //Włączamy pole 5
                                         updateColumn(fromLeft - 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft] = active[fromLeft - 1];                                                                      //Wyłączamy pola 2, 10 i 14
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = active[fromLeft];                                                                      //Włączamy pole 7
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = active[fromLeft];                                                                      //Włączamy pole 8
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromLeft--;                                                                                                   //Zmniejsza się odległość od lewej krawędzi
                                         fromRight -= 2;                                                                                               //Zmniejsza się odległość od prawej krawędzi
                                         krok -= 2;                                                                                                    //Nie ma już żadnych włączonych pól w dwóch dolnych wierszach, więc zmniejszamy krok
                                         angle = 1;                                                                                                    //Bieżący kąt to 1
                                     }
                                     break;

                                 case 1:
                                 // . . . .        . . o .
                                 // o o o o  --->  . . o .
                                 // . . . .        . . o .
                                 // . . . .        . . o .
                                     if((krok < (lcRows - 1))                                                                                          //Czy nie jest to przedostatni krok - bo będziemy dodawać 2 wiersze na dole
                                     && (((active[fromLeft] << 1) | (active[fromLeft] >> 1) | (active[fromLeft] >> 2)) & screen[fromLeft + 2]) == 0)   //Czy pola 3, 11 i 15 są puste
                                     {
                                         active[fromLeft] = 0;                                                                                         //Wyłączamy całą pierwszą kolumnę
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = 0;                                                                                     //Wyłączamy całą drugą kolumnę
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                                                                                                                                       //Włączamy pola 3, 11 i 15
                                         active[fromLeft + 2] = (active[fromLeft + 2] | (active[fromLeft + 2] << 1) | (active[fromLeft + 2] >> 1) | (active[fromLeft + 2] >> 2));
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 3] = 0;                                                                                     //Wyłączamy całą ostatnią kolumnę
                                         updateColumn(fromLeft + 3);                                                                                   //Aktualizacja kolumny
                                         fromLeft += 2;                                                                                                //Zwiększa się odległość od lewej krawędzi
                                         fromRight++;                                                                                                  //Zwiększa się odległość od prawej krawędzi
                                         krok += 2;                                                                                                    //Dołożyliśmy 2 wiersze na dole, więc trzeba zwiększyć krok
                                         angle = 2;                                                                                                    //Bieżący kąt to 2
                                     }
                                     break;

                                 case 2:
                                 // . . o .        . . . .
                                 // . . o .  --->  . . . .
                                 // . . o .        o o o o
                                 // . . o .        . . . .
                                     if((fromLeft > 1)                                                                                                 //Będziemy dodawać 2 kolumny z lewej, więc sprawdzamy, czy nie jesteśmy przy krawędzi lub obok
                                     && (fromRight > 0)                                                                                                //Będziemy dodawać kolumnę z prawej, więc sprawdzamy, czy nie jesteśmy przy krawędzi
                                                                                                                                                       //Czy pola 9, 10 i 12 są puste
                                     && ((((((active[fromLeft] << 1) & (active[fromLeft] >> 2)) & screen[fromLeft - 2]) & screen[fromLeft - 1]) & screen[fromLeft + 1]) == 0))
                                     {
                                         active[fromLeft - 2] = ((active[fromLeft] << 1) & (active[fromLeft] >> 2));                                   //Włączamy pole 9
                                         updateColumn(fromLeft - 2);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft - 1] = active[fromLeft - 2];                                                                  //Włączamy pole 10
                                         updateColumn(fromLeft - 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft] = active[fromLeft - 1];                                                                      //Wyłączamy pola 3, 7 i 15
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = active[fromLeft];                                                                      //Włączamy pole 12
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         fromLeft -= 2;                                                                                                //Zmniejsza się odległość od lewej krawędzi
                                         fromRight--;                                                                                                  //Zmniejsza się odległość od prawej krawędzi
                                         krok--;                                                                                                       //Nie ma już żadnych włączonych pól w dolnym wierszu, więc zmniejszamy krok
                                         angle = 3;                                                                                                    //Bieżący kąt to 3
                                     }
                                     break;

                                 case 3:
                                 // . . . .        . o . .
                                 // . . . .  --->  . o . .
                                 // o o o o        . o . .
                                 // . . . .        . o . .
                                     if((krok < (lcRows))                                                                                              //Czy nie jest to ostatni krok - bo będziemy dodawać wiersz na dole
                                     && (((active[fromLeft] >> 1) | (active[fromLeft] << 1) | (active[fromLeft] << 2)) & screen[fromLeft + 1]) == 0)   //Czy pola 2, 6 i 14 są puste
                                     {
                                         active[fromLeft] = 0;                                                                                         //Wyłączamy całą pierwszą kolumnę
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                                                                                                                                       //Włączamy pola 2, 6 i 14
                                         active[fromLeft + 1] = (active[fromLeft + 1] | (active[fromLeft + 1] >> 1) | (active[fromLeft + 1] << 1) | (active[fromLeft + 1] << 2));
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = 0;                                                                                     //Wyłączamy całą przedostatnią kolumnę
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 3] = 0;                                                                                     //Wyłączamy całą ostatnią kolumnę
                                         updateColumn(fromLeft + 3);                                                                                   //Aktualizacja kolumny
                                         fromLeft++;                                                                                                   //Zwiększa się odległość od lewej krawędzi
                                         fromRight += 2;                                                                                               //Zwiększa się odległość od prawej krawędzi
                                         krok++;                                                                                                       //Dołożyliśmy wiersz na dole, więc trzeba zwiększyć krok
                                         angle = 0;                                                                                                    //Bieżący kąt to 0
                                     }
                                     break;
                             }
                             break;

                         case 6:
                         //"Z"
                             switch(angle)                                                                                                             //Sprawdzamy aktualny kąt
                             {
                                 case 0:
                                 // . . .        . o .
                                 // o o .  --->  o o .
                                 // . o o        o . .
                                     if(((active[fromLeft + 1] & screen[fromLeft]) == 0)                                                               //Czy pole 7 jest puste
                                     && (((active[fromLeft + 1] << 1) & screen[fromLeft + 1]) == 0))                                                   //Czy pole 2 jest puste
                                     {
                                         active[fromLeft] = active[fromLeft + 1];                                                                      //Włączamy pole 7
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = (active[fromLeft + 1] << 1);                                                           //Włączamy pole 2, wyłączamy 8
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = 0;                                                                                     //Wyłączamy całą ostatnią kolumnę
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromRight++;                                                                                                  //Zwiększa się odległość od prawej krawędzi
                                         angle = 1;                                                                                                    //Bieżący kąt to 1
                                     }
                                     break;

                                 case 1:
                                 // . o .        o o .
                                 // o o .  --->  . o o
                                 // o . .        . . .
                                     if((fromRight > 0)                                                                                                //Będziemy dodawać kolumnę z prawej, więc sprawdzamy, czy nie jesteśmy przy krawędzi
                                     && ((((active[fromLeft] << 2) & (active[fromLeft] << 1)) & screen[fromLeft]) == 0)                                //Czy pole 1 jest puste
                                     && (((active[fromLeft] & active[fromLeft + 1]) & screen[fromLeft + 2]) == 0))                                     //Czy pole 6 jest puste
                                     {
                                         active[fromLeft] = ((active[fromLeft] << 2) & (active[fromLeft] << 1));                                       //Włączamy pole 1, wyłączamy 4 i 7
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         //active[fromLeft + 1]                                                                                        //Tu się nic nie zmienia
                                         active[fromLeft + 2] = (active[fromLeft] >> 1);                                                               //Włączamy pole 6
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromRight--;                                                                                                  //Zmniejsza się odległość od prawej krawędzi
                                         krok--;                                                                                                       //Nie ma już żadnych włączonych pól w dolnym wierszu, więc zmniejszamy krok
                                         angle = 2;                                                                                                    //Bieżący kąt to 2
                                     }
                                     break;

                                 case 2:
                                 // o o .        . . o
                                 // . o o  --->  . o o
                                 // . . .        . o .
                                     if((krok < lcRows)                                                                                                //Czy nie jest to ostatni krok - bo będziemy dodawać wiersz na dole
                                     && (((active[fromLeft + 1] >> 1) & screen[fromLeft + 1]) == 0)                                                    //Czy pole 8 jest puste
                                     && (((active[fromLeft + 2] << 1) & screen[fromLeft + 2]) == 0))                                                   //Czy pole 3 jest puste
                                     {
                                         active[fromLeft] = 0;                                                                                         //Wyłączamy całą pierwszą kolumnę
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = (active[fromLeft + 1] >> 1);                                                           //Wyłączamy pole 2, włączamy 8
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = (active[fromLeft + 2] | (active[fromLeft + 2] << 1));                                  //Włączamy pole 3
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromLeft++;                                                                                                   //Zwiększa się odległość od lewej krawędzi
                                         krok++;                                                                                                       //Dołożyliśmy wiersz na dole, więc trzeba zwiększyć krok
                                         angle = 3;                                                                                                    //Bieżący kąt to 3
                                     }
                                     break;

                                 case 3:
                                 // . . o        . . .
                                 // . o o  --->  o o .
                                 // . o .        . o o
                                     if((fromLeft > 0)                                                                                                 //Będziemy dodawać kolumnę z lewej, więc sprawdzamy, czy nie jesteśmy przy krawędzi
                                     && (((active[fromLeft] & active[fromLeft + 1]) & screen[fromLeft - 1]) == 0)                                      //Czy pole 4 jest puste
                                     && (((active[fromLeft + 1] >> 1) & screen[fromLeft + 1]) == 0))                                                   //Czy pole 9 jest puste
                                     {
                                         active[fromLeft - 1] = (active[fromLeft] & active[fromLeft + 1]);                                             //Włączamy pole 4
                                         updateColumn(fromLeft - 1);                                                                                   //Aktualizacja kolumny
                                         //active[fromLeft]                                                                                            //Tu się nic nie zmienia
                                         active[fromLeft + 1] = (active[fromLeft - 1] >> 1);                                                           //Włączamy pole 9, wyłączamy 3 i 6
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         fromLeft--;                                                                                                   //Zmniejsza się odległość od lewej krawędzi
                                         angle = 0;                                                                                                    //Bieżący kąt to 0
                                     }
                                     break;
                             }
                             break;

                         case 7:
                         //"S"
                             switch(angle)                                                                                                             //Sprawdzamy aktualny kąt
                             {
                                 case 0:
                                 // . . .        o . .
                                 // . o o  --->  o o .
                                 // o o .        . o .
                                     if(((active[fromLeft + 1] << 1) & screen[fromLeft]) == 0)                                                         //Czy pola 1 i 4 są puste
                                     {
                                         active[fromLeft] = (active[fromLeft + 1] << 1);                                                               //Włączamy pole 1, wyłączamy 7
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         //active[fromLeft + 1]                                                                                        //Tu się nic nie zmienia
                                         active[fromLeft + 2] = 0;                                                                                     //Wyłączamy całą ostatnią kolumnę
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromRight++;                                                                                                  //Zwiększa się odległość od prawej krawędzi
                                         angle = 1;                                                                                                    //Bieżący kąt to 1
                                     }
                                     break;

                                 case 1:
                                 // o . .        . o o
                                 // o o .  --->  o o .
                                 // . o .        . . .
                                     if((fromRight > 0)                                                                                                //Będziemy dodawać kolumnę z prawej, więc sprawdzamy, czy nie jesteśmy przy krawędzi
                                     && (((active[fromLeft + 1] << 1) & screen[fromLeft + 1]) == 0)                                                    //Czy pole 2 jest puste
                                     && (((active[fromLeft] & (active[fromLeft] << 1)) & screen[fromLeft + 2]) == 0))                                  //Czy pole 3 jest puste
                                     {
                                         active[fromLeft] = (active[fromLeft] & active[fromLeft + 1]);                                                 //Wyłączamy pole 1
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         active[fromLeft + 1] = (active[fromLeft + 1] << 1);                                                           //Włączamy pole 2, wyłączamy 8
                                         updateColumn(fromLeft + 1);                                                                                   //Aktualizacja kolumny
                                         active[fromLeft + 2] = (active[fromLeft] << 1);                                                               //Włączamy pole 3
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromRight--;                                                                                                  //Zmniejsza się odległość od prawej krawędzi
                                         krok--;                                                                                                       //Nie ma już żadnych włączonych pól w dolnym wierszu, więc zmniejszamy krok
                                         angle = 2;                                                                                                    //Bieżący kąt to 2
                                     }
                                     break;

                                 case 2:
                                 // . o o        . o .
                                 // o o .  --->  . o o
                                 // . . .        . . o
                                     if((krok < lcRows)                                                                                                //Czy nie jest to ostatni krok - bo będziemy dodawać wiersz na dole
                                     && (((active[fromLeft + 1] >> 1) & screen[fromLeft + 2]) == 0))                                                   //Czy pola 6 i 9 są puste
                                     {
                                         active[fromLeft] = 0;                                                                                         //Wyłączamy całą ostatnią kolumnę
                                         updateColumn(fromLeft);                                                                                       //Aktualizacja kolumny
                                         //active[fromLeft + 1]                                                                                        //Tu się nic nie zmienia
                                         active[fromLeft + 2] = (active[fromLeft + 1] >> 1);                                                           //Włączamy pola 6 i 9
                                         updateColumn(fromLeft + 2);                                                                                   //Aktualizacja kolumny
                                         fromLeft++;                                                                                                   //Zwiększa się odległość od lewej krawędzi
                                         krok++;                                                                                                       //Dołożyliśmy wiersz na dole, więc trzeba zwiększyć krok
                                         angle = 3;                                                                                                    //Bieżący kąt to 3
                                     }
                                     break;

                                 case 3:
                                 // . o .        . . .
                                 // . o o  --->  . o o
                                 // . . o        o o .
                                     if((fromLeft > 0)                                                                                                //Będziemy dodawać kolumnę z lewej, więc sprawdzamy, czy nie jesteśmy przy krawędzi 
                                     && ((active[fromLeft + 1] & ((active[fromLeft + 1] >> 1)) & screen[fromLeft - 1]) == 0)                          //Czy pole 7 jest puste
                                     && ((active[fromLeft + 1] & screen[fromLeft]) == 0))                                                             //Czy pole 8 jest puste
                                     {
                                         active[fromLeft - 1] = (active[fromLeft + 1] & (active[fromLeft + 1] >> 1));                                 //Włączamy pole 7
                                         updateColumn(fromLeft - 1);                                                                                  //Aktualizacja kolumny
                                         active[fromLeft] = active[fromLeft + 1];                                                                     //Wyłączamy pole 2, włączamy 8
                                         updateColumn(fromLeft);                                                                                      //Aktualizacja kolumny
                                         active[fromLeft + 1] = (active[fromLeft - 1] << 1);                                                          //Wyłączamy pole 9
                                         updateColumn(fromLeft + 1);                                                                                  //Aktualizacja kolumny
                                         fromLeft--;                                                                                                  //Zmniejsza się odległość od lewej krawędzi
                                         angle = 0;                                                                                                   //Bieżący kąt to 0
                                     }
                                     break;
                             }
                             break;
                     }
                     playNote('E', 10);
                     buttonDelay(200);                                                                                                                //Opóźnienie, żeby nie obracać za wiele za jednym naciśnięciem buttona
                 }
             }



//==Restart=====================================================================

             //Restart
             if(digitalRead(start_button) == HIGH)                  //Jeśli guzik "Start/restart" jest wciśnięty
             {
                 memset(lc0, 0, sizeof(lc0));                       //Zerowanie górnego ledcontrola
                 memset(lc1, 0, sizeof(lc1));                       //Zerowanie dolnego ledcontrola
                 memset(active, 0, sizeof(active));                 //Zerowanie aktywnego obrazu
                 memset(screen, 0, sizeof(screen));                 //Zerowanie obrazu tła
                 score = 0;                                         //Zerowanie punktów
                 allLines = 0;                                      //Zerowanie linii
                 figura = 0;                                        //Zerowanie wylosowanej figury
                 break;
             }



//==Przesunięcie w dół==========================================================

             //Sprawdzanie mozliwosci przesuniecia w dol
             for(int colnum = 0; colnum < lcCols; colnum++)                      //Pętla po wszystkich kolumnach
             {
                 //Jesli po przesunieciu nie wjedziemy na inne klocki
                 if((screen[colnum] & (active[colnum] >> 1)) == 0)               //Jeśli nie ma kolizji, to iloczyn bitowy dwóch obrazów da 0
                 {
                     colCheck++;
                 }
                 //Jesli w tej kolumnie nie ma jeszcze nic
                 /*else if((screen[colnum] == 0) && ((active[colnum] & 1) == 1))   //Jeśli obraz tła jest pusty oraz aktywny obraz jest na samym dole (jest nieparzysty, bo na dole jest jedynka, czyli dotarł do samego dołu)
                 {
                     colCheck++;
                 }*/
                 //Jesli przesuwana kolumna nie zawiera spadajacego klocka (jest pusta)
                 /*else if(active[colnum] == 0)
                 {
                     colCheck++;
                 }*/
                 else
                 {
                     colCheck = 0;
                     if(krok == 0)                                               //Jeśli nie da się przesunąć, a to jest dopiero pierwszy obrót pętli, to znaczy, że plansza jest pełna --> Game over!
                     {
                         started = 0;
                     }
                 }
             }

             //Jeśli można przesunąć
             if((colCheck == lcCols) && (krok < lcRows))           //Jeśli colCheck jest równy lcCols, to znaczy, że każda kolumna przeszła weryfikację i można przesunąć cały obraz o linię w dół. Krok musi być mniejszy niż ilość wierszy
             {
                 for(int colnum = 0; colnum < lcCols; colnum++)    //Pętla po wszystkich kolumnach
                 {
                     active[colnum] = active[colnum] >> 1;         //Bitowe przesunięcie w prawo da przesunięcie kolumny w dół
                     updateColumn(colnum);
                 }
             }
             else
             {
                 break;                                            //Jeśli nie można przesunąć, to przerywamy, żeby szybciej przejść do następnego klocka
             }             
             delay(brickDelay);   //Opóźnienie spadania
         }
//---> Koniec pętli z krokami


//==Aktualizacja tła============================================================

         //Po zatrzymaniu się klocka jest dodawany do tych, co juz są na planszy --> Staje się elementem tła
         for(int colnum = 0; colnum < lcCols; colnum++)                         //Pętla po kolumnach w każdym ledcontrolu
         {
             screen[colnum] = screen[colnum] | (lc0[colnum] << (lcRows / 2));   //Dodanie do tła zawartości ledcontola
             screen[colnum] = screen[colnum] | lc1[colnum];                     //Dodanie do tła zawartości ledcontola
             lc0[colnum] = 0;                                                   //Zerowanie
             lc1[colnum] = 0;                                                   //Zerowanie
             active[colnum] = 0;                                                //Zerowanie
         }



//==Kasowanie linii i punkty====================================================

         //Kasowanie pełnych linii i zwiększanie wyniku
         currLines = 0;                                                                //Licznik skasowanych linii za jednym razem przy bieżącym klocku
         for(int rownum = 0; rownum < lcRows; rownum++)                                //Pętla po wszystkich wierszach
         {
             colCheck = 0;                                                             //Zerowanie sprawdzacza kolumn
             for(int colnum = 0; colnum < lcCols; colnum++)                            //Pętla po wszystkich kolumnach
             {
                 if(((screen[colnum] >> rownum) & 1) == 1)                             //Jeśli po przesunięciu kolumny o numer bieżącego wiersza i iloczynie logicznym z 1 otrzymamy 1, to znaczy że dioda jest zapalona w tym miejscu
                 {
                     colCheck++;                                                       //Zwiększamy sprawdzacz
                 }
             }
             if(colCheck == lcCols)                                                    //Jeśli sprawdzacz == ilości wszystkich kolumn, to znaczy, że wszystkie są zapalone --> to jest pełna linia
             {
                 //Animacja kasowania
                 for(int colnum = 0; colnum < lcCols; colnum++)                                       //Pętla po wszystkich kolumnach
                 {
                     tmpCol = ~((int) round(pow(2, rownum)));                                         //Maska bitowa --> 0 na pozycji odpowiadającej linii do skasowania, reszta bitów 1
                     screen[colnum] = screen[colnum] & tmpCol;                                        //Użycie maski bitowej na kolumnie
                     updateColumn(colnum);                                                            //Aktualizacja ekranu
                     
                     switch(currLines)                                                                //Wybór nuty do odegrania przy kasowaniu. Im więcej linii tym następny dźwięk jest wyższy --> to już typowo dla szpanu ;)
                     {
                         case 0:
                             playNote('b', 20);
                             break;
                         case 1:
                             playNote('D', 20);
                             break;
                         case 2:
                             playNote('F', 20);
                             break;
                         case 3:
                             playNote('A', 20);
                             break;
                     }
                     delay(30);                                                                       //Opóźnienie, żeby w ogóle dało się zaobserować efekt

                     //Rzeczywiste usunięcie linii
                     tmpCol = (int) (round(pow(2, rownum)) - 1);                                      //Maska bitowa --> "dół" z bieżącej kolumny, który jest pod linią do skasowania
                     tmpCol = screen[colnum] & tmpCol;                                                //Tymczasowa kolumna ma zawierać tylko ten "dół"
                     screen[colnum] = (screen[colnum] >> (rownum + 1));                               //Przesuwamy bieżącą kolumnę w dół, aby usunąć linię i to co pod spodem
                     screen[colnum] = (screen[colnum] << rownum);                                     //Przesuwamy bieżącą kolumnę w górę, ale o 1 mniej (bo usunięto linię)
                     screen[colnum] = screen[colnum] | tmpCol;                                        //Dodajemy do bieżącej kolumny to, co było pod skasowaną linią
                     
                 }
                 
                 //Efekt "spadnięcia" klocków po usunięciu linii
                 for(int colnum = 0; colnum < lcCols; colnum++)                                       //Pętla po wszystkich kolumnach
                 {
                     updateColumn(colnum);                                                            //Aktualizacja ekranu
                 }
                 rownum--;                                                                            //Zmniejszamy licznik w pętli, bo zmieniła się numeracja wierszy i trzeba sprawdzić jeszcze raz (może być kolejna linia)
                 currLines++;                                                                         //Zwiększamy licznik bieżących linii
                 allLines++;                                                                          //Zwiększamy licznik wszystkich linii
             }
         }
         
         if(currLines > 0)                                                                            //Jeśli były jakieś skasowane linie
         {
             score += (int) round(pow(4, currLines-1));                                               //Punktacja
         }
    }


     
    
     

//==Game over===================================================================
     
 }
}
